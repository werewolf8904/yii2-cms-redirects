<?php

namespace werewolf8904\cmsredirects\controllers;


use werewolf8904\cmscore\controllers\BackendController;
use werewolf8904\cmsredirects\models\search\RedirectsSearch;
use werewolf8904\cmsredirects\models\Redirects;

/**
 * RedirectsController implements the CRUD actions for Redirects model.
 */
class RedirectsController extends BackendController
{
    
    public $pk='from';
    public $searchClass = RedirectsSearch::class;
    public $class = Redirects::class;
}
