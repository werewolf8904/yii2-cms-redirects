<?php

namespace werewolf8904\cmsredirects;

use Yii;

/**
 * redirects module definition class
 */
class Module extends \yii\base\Module
{


    /**
     * Host info that will be removed from begin of redirects string
     * @var string
     */
    public $hostInfo;

    public function init()
    {
        parent::init();
        $this->registerTranslations();
    }

    public function registerTranslations()
    {
        $i18n = Yii::$app->i18n;
        $i18n->translations['cms.redirects'] = [
            'class' => 'yii\i18n\PhpMessageSource',
            'sourceLanguage' => 'en',
            'basePath' => __DIR__.'/messages',
        ];
    }
}
