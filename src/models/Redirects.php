<?php

namespace werewolf8904\cmsredirects\models;

use werewolf8904\cmsredirects\Module;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "redirects".
 *
 * @property string  $from
 * @property integer $status
 * @property string  $to
 * @property string  $type
 */
class Redirects extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%redirects}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['from'], 'required'],
            [['from','to'],'filter','filter'=>function($value)
            {
                $hostInfo=Module::getInstance()->hostInfo;
                $value=ltrim($value,$hostInfo);
                $value=trim($value,'/ ');

                return $value;
            }],
            [['from'], 'unique'],
            [['status'], 'integer'],
            ['status', 'default', 'value' => 1],
            [['from', 'to'], 'string', 'max' => 255],
            [['type'], 'string', 'max' => 50]
        ];
    }

    /**
     * This method is called at the end of inserting or updating a record.
     * The default implementation will trigger an [[EVENT_AFTER_INSERT]] event when `$insert` is `true`,
     * or an [[EVENT_AFTER_UPDATE]] event if `$insert` is `false`. The event class used is [[AfterSaveEvent]].
     * When overriding this method, make sure you call the parent implementation so that
     * the event is triggered.
     *
     * @param bool  $insert            whether this method called while inserting a record.
     *                                 If `false`, it means the method is called while updating a record.
     * @param array $changedAttributes The old values of attributes that had changed and were saved.
     *                                 You can use this parameter to take action based on the changes made for example
     *                                 send an email when the password had changed or implement audit trail that tracks
     *                                 all the changes.
     *                                 `$changedAttributes` gives you the old attribute values while the active record
     *                                 (`$this`) has already the new, updated values.
     *
     * Note that no automatic type conversion performed by default. You may use
     * [[\yii\behaviors\AttributeTypecastBehavior]] to facilitate attribute typecasting.
     * See http://www.yiiframework.com/doc-2.0/guide-db-active-record.html#attributes-typecasting.
     */
    public function afterSave($insert, $changedAttributes)
    {
        //TODO Make cache extension configurable
        parent::afterSave($insert, $changedAttributes); // TODO: Change the autogenerated stub
        Yii::$app->cache->delete('redirects');
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'from' => Yii::t('cms.redirects', 'From'),
            'status' => Yii::t('cms.redirects', 'Status'),
            'to' => Yii::t('cms.redirects', 'To'),
            'type' => Yii::t('cms.redirects', 'Type')
        ];
    }
}
