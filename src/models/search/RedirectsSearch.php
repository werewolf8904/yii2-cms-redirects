<?php

namespace werewolf8904\cmsredirects\models\search;



use werewolf8904\cmsredirects\models\Redirects;
use yii\data\ActiveDataProvider;

/**
 * Redirects represents the model behind the search form about `common\modules\redirects\models\Redirects`.
 */
class RedirectsSearch extends Redirects
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['from', 'to', 'type'], 'safe'],
            [['status'], 'integer']
        ];
    }


    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     * @throws \yii\base\InvalidArgumentException
     */
    public function search($params)
    {
        $query = Redirects::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'status' => $this->status
        ]);

        $query->andFilterWhere(['like', 'from', $this->from])
            ->andFilterWhere(['like', 'to', $this->to])
            ->andFilterWhere(['like', 'type', $this->type]);

        return $dataProvider;
    }
}
