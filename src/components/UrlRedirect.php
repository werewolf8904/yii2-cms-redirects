<?php
/**
 * Created by PhpStorm.
 * User: Prong
 * Date: 01.08.2016
 * Time: 10:35
 */

namespace werewolf8904\cmsredirects\components;

use werewolf8904\cmsredirects\models\Redirects;
use Yii;
use yii\base\BaseObject;
use yii\base\InvalidConfigException;
use yii\web\UrlManager;
use yii\web\UrlNormalizer;
use yii\web\UrlRuleInterface;

/**
 * Class UrlRedirect
 * @package common\modules\url\components
 */
//TODO Make cache extension configurable
class UrlRedirect extends BaseObject implements UrlRuleInterface
{

    protected static $redirects;
    public $normalizer;

    public function init()
    {
        if (\is_array($this->normalizer)) {
            $normalizerConfig = array_merge(['class' => UrlNormalizer::class], $this->normalizer);
            $this->normalizer = Yii::createObject($normalizerConfig);
        }
        if ($this->normalizer !== null && $this->normalizer !== false && !$this->normalizer instanceof UrlNormalizer) {
            throw new InvalidConfigException('Invalid config for UrlRule::normalizer.');
        }

    }

    /**
     * @param UrlManager $manager
     * @param string     $route
     * @param array      $params
     *
     * @return bool|string
     */
    public function createUrl($manager, $route, $params)
    {
        return false;
    }

    /**
     * @param UrlManager       $manager
     * @param \yii\web\Request $request
     *
     * @return array|bool
     * @throws \yii\base\ExitException
     * @throws \yii\base\InvalidConfigException
     */
    public function parseRequest($manager, $request)
    {
        if (static::$redirects === null) {
            static::$redirects = Yii::$app->cache->getOrSet('redirects', function () {
                return Redirects::find()->where(['status' => 1])->indexBy('from')->asArray()->all();
            }, 0);
        }

        $pathInfo = rtrim($request->getPathInfo(), $manager->suffix);

        if (array_key_exists($pathInfo, static::$redirects)) {
            $redirect = static::$redirects[$pathInfo]['to'];
            $type = static::$redirects[$pathInfo]['type'];
            $normalized = false;
            if ($this->hasNormalizer($manager)) {
                $redirect = $this->getNormalizer($manager)->normalizePathInfo($redirect, $manager->suffix, $normalized);
            }
            Yii::$app->response->redirect('/' . $redirect, $type);
            Yii::$app->end();
        }
        return false;
    }

    /**
     * @param UrlManager $manager the URL manager
     *
     * @return bool
     * @since 2.0.10
     */
    protected function hasNormalizer($manager)
    {
        return $this->getNormalizer($manager) instanceof UrlNormalizer;
    }

    /**
     * @param $manager
     *
     * @return mixed
     */
    protected function getNormalizer($manager)
    {
        if ($this->normalizer === null) {
            return $manager->normalizer;
        }

        return $this->normalizer;
    }


}