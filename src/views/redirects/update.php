<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model \werewolf8904\cmsredirects\models\Redirects */

$this->title = Yii::t('cms.redirects', 'Update {modelClass}: ', [
    'modelClass' => Yii::t('cms.redirects', 'Redirects'),
]) . ' ' . $model->from;
$this->params['breadcrumbs'][] = ['label' => Yii::t('cms.redirects', 'Redirects'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('cms.redirects', 'Update');
?>
<div class="redirects-update">

    <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
