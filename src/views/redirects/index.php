<?php

use kartik\editable\Editable;
use kartik\grid\GridView;
use yii\helpers\Html;

//use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel \werewolf8904\cmsredirects\models\Redirects */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('cms.redirects', 'Redirects');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="redirects-index">

    <p>
        <?php echo Html::a(Yii::t('cms.redirects', 'Create {modelClass}', [
            'modelClass' => Yii::t('cms.redirects', 'Redirects'),
        ]), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'from',
            'to',
            'type',
            'status',

            [
//                'header' => 'Actions',
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update}{delete}',

            ],
        ],
    ]);
    ?>
</div>
