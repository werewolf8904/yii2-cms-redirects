<?php




/* @var $this yii\web\View */
/* @var $model \werewolf8904\cmsredirects\models\Redirects */

$this->title = Yii::t('cms.redirects', 'Create {modelClass}', [
    'modelClass' => Yii::t('cms.redirects', 'Redirects'),
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('cms.redirects', 'Redirects'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="redirects-create">

    <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
