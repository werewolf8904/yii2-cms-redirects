<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model \werewolf8904\cmsredirects\models\Redirects */
/* @var $form yii\bootstrap\ActiveForm */
?>

<div class="redirects-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php echo $form->errorSummary($model); ?>

    <?php echo $form->field($model, 'from')->textInput(['maxlength' => true]) ?>

    <?php echo $form->field($model, 'status')->checkbox() ?>

    <?php echo $form->field($model, 'to')->textInput(['maxlength' => true]) ?>



    <?php echo $form->field($model, 'type')->dropDownList(['302'=>'302','301'=>'301']) ?>

    <div class="form-group">
        <?php echo Html::submitButton($model->isNewRecord ? Yii::t('cms.redirects', 'Create') : Yii::t('cms.redirects', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
