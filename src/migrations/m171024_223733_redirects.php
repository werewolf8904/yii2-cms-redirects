<?php


class m171024_223733_redirects extends \werewolf8904\cmscore\db\Migration
{
    public function safeUp()
    {
        $this->createTable('{{%redirects}}', [
            'from' => $this->string()->notNull(),
            'status' => $this->smallInteger(1)->notNull()->defaultValue(1),
            'to' => $this->string()->notNull(),
            'type' => $this->string(50),
        ], $this->tableOptions);

        $this->addPrimaryKey('idx_redirects', '{{%redirects}}', 'from');
    }

    public function safeDown()
    {
        $this->dropTable('{{%redirects}}');
    }
}
